import os
import datetime
from b2blaze import B2
b2 = B2(key_id=os.environ.get('B2_KEY'), application_key=os.environ.get('B2_APPLICATION'))
bucket = b2.buckets.get(os.environ.get('B2_BUCKET'))
files = bucket.files.all()

# CREATE TABLE files (id BIGINT(20) UNSIGNED NOT NULL PRIMARY KEY, mimeType ENUM("image/jpeg"), path VARCHAR(255))

#import mysql.connector
#cnx = mysql.connector.connect(user='cloud-images', password='cloud-images', host='127.0.0.1', database='cloud-images')
#cursor = cnx.cursor()

import sqlite3
db = sqlite3.connect(os.environ.get('DB'))

def executeQuery(db, query, data):
    db.execute(query, data)

def fetchColumn(db, query, data):
    values = []
    c = db.cursor()
    c.execute(query, data)
    row = c.fetchone()
    while row:
        values.append( str(row[0]) )
        row = c.fetchone()
    return values

currentFileIds = fetchColumn(db, 'select id from files', ())


data = []
query1 = 'REPLACE INTO files (id, mimeType, path, originalFilename) VALUES'
tupleSize = 4
query2 = '(%d,%d,%d,%d),'
query2 = '(?,?,?,?),'
for file in files:
    # skip folders
    if file.action == 'folder':
        continue

    # extract file id from name
    fileId = file.file_name[8:25]
    # TODO: this timestamp is wrong
    if fileId == '20170430241125191':
        continue
    if fileId == '':
        continue

    if fileId in currentFileIds:
        data = (
            file.content_type,
            file.file_name,
            file.file_info['original-filename'] if 'original-filename' in file.file_info else '',
            fileId
        )
        db.execute('UPDATE files SET mimeType=?,path=?,originalFilename=? WHERE id=?', data)
    else:
        data = (
            fileId,
            file.content_type,
            file.file_name,
            file.file_info['original-filename'] if 'original-filename' in file.file_info else ''
        )
        db.execute('INSERT INTO files (id, mimeType, path, originalFilename) VALUES (?,?,?,?)', data)


db.commit()
#cnx.close()
db.close()
