from b2blaze import B2
import datetime
import sys
import os
import re
from PIL import Image
from PIL.ExifTags import TAGS
import subprocess

b2 = B2(key_id=os.environ.get('B2_KEY'), application_key=os.environ.get('B2_APPLICATION'))
bucket = b2.buckets.get(os.environ.get('B2_BUCKET'))

print(sys.argv[1])

def iterateOverFiles(root):
    for dirpath, dirnames, filenames in os.walk(root):
        for f_name in filenames:
            yield os.path.join(dirpath, f_name)

def get_exif(filepath):
  i = Image.open(filepath)
  info = i._getexif()
  if info:
      return {TAGS.get(tag): value for tag, value in info.items()}
  return {}


b2files = {}
for f in bucket.files.all():
    b2files[ f.file_name ] = f
#print(b2files)

for filepath in iterateOverFiles(sys.argv[1]):
    stat = os.stat(filepath)

    filename = os.path.basename(filepath)

    mimeType = ''
    extension = ''
    folder = ''
    timestampFilename = ''
    modifiedMillis = 0

    # LUMIX
    # Use portion of incrementing integer as millis value
    # to work around images taken within the same second
    # P1020057.JPG
    if re.search("^P\d{7}\.JPG$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"

        exif_data = get_exif(filepath)

        if 'DateTimeOriginal' not in exif_data:
            console.log('JPG has no EXIF data: ' + filepath)
            continue

        # 2017:11:23 12:49:01
        taken = exif_data['DateTimeOriginal']
        folder = taken[0:4] + '/' + taken[5:7]
        # Work around same-second photos by appending last 3 of filename as millis
        timestampFilename = taken[0:4] + taken[5:7] + taken[8:10] + taken[11:13] + taken[14:16] + taken[17:19] + filename[5:8]

    elif re.search("^P\d{7}\.MP4$", filename):
        mimeType = "video/mp4"
        extension = "mp4"
        continue
        img = PIL.Image.open(filepath)
        exif_data = img._getexif()
        # bail if failed

        #console.log('taken at ' + metadata['exif']['DateTimeOriginal']);
        #var d = moment( metadata['exif']['DateTimeOriginal']);
        #d.millisecond(parseInt(filename.substr(5,3)));
        #folder = d.format('YYYY/MM');
        #// workaround same-second photos by appending last 3 as millis
        #timestampFilename = d.format('YYYYMMDDHHmmssSSS');
        #modifiedMillis = d.valueOf();

    # ANDROID
    # IMG_20160906_104533196.jpg
    elif re.search("^IMG_\d{8}_\d{9}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        folder = filename[4:8] + '/' + filename[8:10]
        timestampFilename = filename[4:12] + filename[13:22]
    elif re.search("^IMG_\d{8}_\d{9}(_HDR|_TOP)\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        # Add 1 to the milliseconds of these files to avoid conflicts with non TOP/HDR files
        folder = filename[4:8] + '/' + filename[8:10]
        timestampFilename = filename[4:12] + filename[13:19] + ("%03d" % (int(filename[19:22]) + 1))
    elif re.search("^VID_\d{8}_\d{9}\.mp4$", filename):
        mimeType = "video/mp4"
        extension = "mp4"
        folder = filename[4:8] + '/' + filename[8:10]
        timestampFilename = filename[4:12] + filename[13:22]

    # Older android
    # IMG_20160906_104533.jpg
    elif re.search("^IMG_\d{8}_\d{6}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        folder = filename[4:8] + '/' + filename[8:10]
        timestampFilename = filename[4:12] + filename[13:19] + '000'
    # PANO_20131225_163350.jpg to: _201/31/_2013122_16335000.jpg
    elif re.search("^PANO_\d{8}_\d{6}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        # TODO: fix offsets
        folder = filename[5:9] + '/' + filename[9:11]
        timestampFilename = filename[5:13] + filename[14:20] + '000'
    elif re.search("^VID_\d{8}_\d{6}\.mp4$", filename):
        mimeType = "video/mp4"
        extension = "mp4"
        folder = filename[4:8] + '/' + filename[8:10]
        timestampFilename = filename[4:12] + filename[13:19] + '000'

    # SPH-D710 - Samsung Galaxy S2
    # 20130627_192631.jpg
    elif re.search("^\d{8}_\d{6}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        folder = filename[0:4] + '/' + filename[4:6]
        timestampFilename = filename[0:8] + filename[9:15] + '000'
    elif re.search("^\d{8}_\d{6}\.mp4$", filename):
        mimeType = "video/mp4"
        extension = "mp4"
        folder = filename[0:4] + '/' + filename[4:6]
        timestampFilename = filename[0:8] + filename[9:15] + '000'

    # Samsung SPH-M910 - Android 2.x
    # 2011-01-11 17.21.28.jpg
    elif re.search("^\d{4}-\d{2}-\d{2} \d{2}\.\d{2}\.\d{2}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        folder = filename[0:4] + '/' + filename[5:7]
        timestampFilename = filename[0:4] + filename[5:7] + filename[8:10] + filename[11:13] + filename[14:16] + filename[17:19] + '000'

    # next
    # VID_20120406_204919.m4v
    # VID_20120203_162419.3gp

    # LG Rumor2
    # p_00064.jpg, have to fetch image metadata for date
    elif re.search("^p_\d{5}\.jpg$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        print('Old file %s' % filename)

        exif_data = get_exif(filepath)

        if 'DateTimeOriginal' not in exif_data:
            print('JPG has no EXIF data: ' + filepath)
            continue

        # 2017:11:23 12:49:01
        taken = exif_data['DateTimeOriginal']
        folder = taken[0:4] + '/' + taken[5:7]
        # Work around same-second photos by appending last 3 of filename as millis
        timestampFilename = taken[0:4] + taken[5:7] + taken[8:10] + taken[11:13] + taken[14:16] + taken[17:19] + filename[2:7]

    # Casio camera
    # CIMG1234.JPG, have to fetch image metadata for date
    elif re.search("^CIMG\d{4}\.JPG$", filename):
        mimeType = "image/jpeg"
        extension = "jpg"
        print('Old file %s' % filename)

        exif_data = get_exif(filepath)

        if 'DateTimeOriginal' not in exif_data:
            print('JPG has no EXIF data: ' + filepath)
            continue

        # 2017:11:23 12:49:01
        taken = exif_data['DateTimeOriginal']
        folder = taken[0:4] + '/' + taken[5:7]
        # Work around same-second photos by appending last 3 of filename as millis
        timestampFilename = taken[0:4] + taken[5:7] + taken[8:10] + taken[11:13] + taken[14:16] + taken[17:19] + filename[5:8]

    else:
        print('NEW FILENAME PATTERN, skipping: ' + filename)
        continue


    path = folder + '/' + timestampFilename + '.' + extension

    # Does file already exists in the list we fetched?
    if path in b2files:
        b2file = b2files[path]

        if stat.st_size == b2file.content_length:
            print('Identical file exists in b2. Skipping ' + filepath + ' to: ' + path)
        else:
            print('SIZE CONFLICT. %s to %s. Sizes: %d and %d' %(filepath, path, stat.st_size, b2file.content_length))
            #filedata = open(filepath, 'rb')
            #bucket.files.upload(contents=filedata, file_name=path, mime_content_type=mimeType)
    else:
        #2upload(bucketId, filepath, path, mimeType, modifiedMillis, fileStats, next)
        print('Uploading ' + filepath + ' to: ' + path)
        #filedata = open(filepath, 'rb')
        #bucket.files.upload(contents=filedata, file_name=path, mime_content_type=mimeType)

        # upload using CLI
        #result = subprocess.run(["b2", "upload-file", "--contentType", mimeType, 'szlosek-photos-videos', filepath, path], capture_output=True)
        result = subprocess.run(["b2", "upload-file", "--contentType", mimeType, "--info", "original-filename=" + filename, 'szlosek-photos-videos', filepath, path], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0:
            print('Failed to upload %s: %s %s' % (filepath, result.stdout, result.stderr))
