import os
from flask import Flask, g, request, render_template, make_response, redirect
import db
import json
import signal
import threading
from PIL import Image
from PIL.ExifTags import TAGS
from b2blaze import B2


app = Flask(__name__)
app.config['IMAGE_CACHE_DIR'] = os.environ.get('IMAGE_CACHE_DIR')
app.config['DOMAIN'] = os.environ.get('DOMAIN')
#if os.environ.get('ENV') == 'dev':
#    app.config['DB_LIMIT'] = 5
#else:
app.config['DB_LIMIT'] = 100

b2 = B2(key_id=os.environ.get('B2_KEY'), application_key=os.environ.get('B2_APPLICATION'))
bucket = b2.buckets.get(os.environ.get('B2_BUCKET'))
db.init_app(app)

def mimeTypeToExtension(mimeType):
    map = {
        'image/jpeg': 'jpg'
    }
    return map[mimeType]

def get_exif(filepath):
    i = Image.open(filepath)
    info = i._getexif()
    if info:
        return {TAGS.get(tag): value for tag, value in info.items()}
    return {}


imageCleanupThreadEvent = threading.Event()
class ImageCleanupThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event

    def run(self):
        blip = 0
        while not self.stopped.wait(5.0):
            print("thread")
            blip+=1
image_cleanup_thread = ImageCleanupThread(imageCleanupThreadEvent)
image_cleanup_thread.start()

def signal_handler(sig, frame):
    global imageCleanupThreadEvent
    print('signal')
    imageCleanupThreadEvent.set()
signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGINT, signal_handler)


@app.route("/")
def hello():
    return "Hello World!"

@app.route("/admin")
def admin():
    return render_template('admin.html')

@app.route("/api/init")
def apiInit():
    out = {
        'firstYear': 2010,
        'firstDate': '2010-01-01',
        'lastYear': 2010,
        'lastDate': '2010-01-01'
    }
    c = db.get_db()
    for row in c.execute("SELECT min(id) as minDate, max(id) as maxDate FROM files"):
        a = str(row[0])
        b = str(row[1])
        out['firstYear'] = a[0:4]
        out['firstDate'] = a[0:4] + '-' + a[4:6] + '-' + a[6:8]
        out['lastYear'] = b[0:4]
        out['lastDate'] = b[0:4] + '-' + b[4:6] + '-' + b[6:8]
    # Return JSON
    return json.dumps(out)

@app.route("/api/files")
def apiFiles():
    out = []

    prefix = None
    if 'prefix' in request.args:
        prefix = request.args['prefix']
    tags = []
    if 'tags' in request.args:
        tags = request.args['tags']
        if len(tags) > 0:
            tags = [str(int(x)) for x in tags.split(',')]
        print(tags)
    if 'offset' in request.args:
        offset = int(request.args['offset'])
    else:
        offset = 0
    if 'limit' in request.args:
        limit = min(int(request.args['limit']), app.config['DB_LIMIT'])
    else:
        limit = app.config['DB_LIMIT']

    # request.args.get('limit') and offset for query param pagination

    c = db.get_db()
    if prefix and len(prefix) == 8:
        a = prefix + '999999999'
        if tags and len(tags) > 0:
            result = c.execute("SELECT files.id, files.path, files.mimeType FROM files LEFT JOIN files_tags ON (files.id=files_tags.fileId) WHERE files.id < ? AND files.mimeType='image/jpeg' AND files_tags.tagId IN (" + ",".join(tags) + ") ORDER BY files.id DESC LIMIT ?,?", (a, offset,limit))
        else:
            result = c.execute("SELECT id, path, mimeType FROM files WHERE id < ? AND mimeType='image/jpeg' ORDER BY id DESC LIMIT ?,?", (a, offset,limit))
    else:
        result = c.execute("SELECT id, path, mimeType FROM files WHERE mimeType='image/jpeg' ORDER BY id DESC LIMIT ?,?", (offset,limit))
    ids = {}
    for row in result:
        id = str(row[0])
        ids[id] = []
        out.append(
            {
                # Large integers seem to mess up JavaScript in browsers
                "id": id,
                "path": row[1],
                "extension": mimeTypeToExtension(row[2]),
                "tags": ids[id]
            }
        )
    result = c.execute("SELECT files_tags.fileId,tags.name FROM files_tags LEFT JOIN tags ON (files_tags.tagId=tags.id) WHERE files_tags.fileId IN (\"" + '","'.join(list(ids.keys())) + "\")")

    for row in result:
        id = str(row[0])
        ids[id].append(row[1])

    # Return JSON
    return json.dumps(out)

@app.route("/api/tags", methods=["GET"])
def apiTagsGet():
    out = []

    c = db.get_db()
    result = c.execute("SELECT id,name FROM tags ORDER BY name")
    for row in result:
        out.append(
            {
                "id": str(row[0]),
                "name": row[1]
            }
        )
    # Return JSON
    return json.dumps(out)

@app.route("/api/tags", methods=["POST"])
def apiTagsPost():
    body = request.get_json()
    print(body['tag'])
    c = db.get_db()
    result = c.execute("INSERT INTO tags (name) VALUES(?)", (body['tag'],))
    c.commit()

    out = []
    result = c.execute("SELECT id,name FROM tags ORDER BY name")
    for row in result:
        out.append(
            {
                "id": str(row[0]),
                "name": row[1]
            }
        )
    # Return JSON
    return json.dumps(out)

@app.route("/img/<filename>")
def image(filename):
    parts = filename.split('.')
    size = None
    if len(parts) == 3: # id.size.extension
        # TODO: Validate this input
        size = parts[1]
        fileExtension = parts[2]
    elif len(parts) == 2: # id.extension
        fileExtension = parts[1]
    else:
        return false
    fileId = parts[0]

    # Fetch image row from database using hash lookup
    d = db.get_db()
    c = d.cursor()
    c.execute("select id, mimeType from files WHERE id=?", (fileId,))
    row = c.fetchone()
    if not row:
        return False
    # Does this file exist?

    fileType = None
    if row[1] == 'image/jpeg':
        fileType = 'jpg'
        fileExtension = '.' + fileType

    if size: # Request for resized image
        tempPath = app.config['IMAGE_CACHE_DIR'] + fileId + '.' + size + fileExtension
        if os.path.isfile(tempPath):
            # Redirect so webserver can serve it
            return redirect(app.config['DOMAIN'] + '/images/' + fileId + '.' + size + fileExtension)

        # Don't have resized version, do we have the original image saved on disk?
        tempPath = app.config['IMAGE_CACHE_DIR'] + fileId + fileExtension
        if not os.path.isfile(tempPath):
            # Get file info
            b2path = fileId[0:4] + '/' + fileId[4:6] + '/' + fileId + fileExtension
            file_by_name = bucket.files.get(file_name=b2path)
            if not file_by_name:
                return False

            # Download to temp directory
            downloaded_file = file_by_name.download()
            imageBytes = downloaded_file.read()
            save_file = open(tempPath, 'wb')
            save_file.write(imageBytes)
            save_file.close()

        print('Resizing')
        resizedFilename = app.config['IMAGE_CACHE_DIR'] + fileId + '.' + size + fileExtension
        maxSizeTuple = size.split('x')
        maxSizeTuple[0] = int(maxSizeTuple[0])
        maxSizeTuple[1] = int(maxSizeTuple[1])

        # Fix orientation
        rotated = False
        # Now get the EXIF tags, specifically Orientation so we know whether to rotate before writing
        exif = get_exif(tempPath)
        im = Image.open(tempPath)
        if 'Orientation' in exif:
            if exif['Orientation'] == 3:
                im = im.rotate(180, expand=True)
            elif exif['Orientation'] == 6:
                im = im.rotate(-90, expand=True)
                rotated = True
            elif exif['Orientation'] == 8:
                im = im.rotate(90, expand=True)
                rotated = True
        if rotated:
            tmp = maxSizeTuple[0]
            maxSizeTuple[0] = maxSizeTuple[1]
            maxSizeTuple[1] = tmp
        im.thumbnail( maxSizeTuple )
        im.save(resizedFilename)

        # Redirect so webserver can serve it
        return redirect(app.config['DOMAIN'] + '/images/' + fileId + '.' + size + fileExtension)


    else: # Request for full-size image
        tempPath = app.config['IMAGE_CACHE_DIR'] + fileId + fileExtension
        if not os.path.isfile(tempPath):
            # Don't have original on disk
            tempPath = app.config['IMAGE_CACHE_DIR'] + fileId + fileExtension
            # Get file info
            b2path = fileId[0:4] + '/' + fileId[4:6] + '/' + fileId + fileExtension
            file_by_name = bucket.files.get(file_name=b2path)
            if not file_by_name:
                return False

            # Download to temp directory
            downloaded_file = file_by_name.download()
            imageBytes = downloaded_file.read()
            save_file = open(tempPath, 'wb')
            save_file.write(imageBytes)
            save_file.close()

            # Now get the EXIF tags, specifically Orientation so we know whether to rotate before writing
            exif = get_exif(tempPath)

        # Redirect so webserver can serve it
        return redirect(app.config['DOMAIN'] + '/images/' + fileId + '.' + fileExtension)


    return False

@app.route("/tag-image/<filename>/<tags>", methods=['POST'])
def tagImage(filename, tags):
    c = db.get_db()
    for tag in tags.split(','):
        print('Tagging %s %s' % (filename, tag))

        result = c.execute("REPLACE INTO files_tags (fileId, tagId) VALUES(?,?)", (filename, tag))
    c.commit()

    # Now return images's current tags so we can update the UI
    result = c.execute("SELECT tags.name FROM files_tags LEFT JOIN tags ON (files_tags.tagId=tags.id) WHERE files_tags.fileId=?", (filename,))
    out = []
    for row in result:
        out.append(row[0])
    # Return JSON
    return json.dumps(out)
    #return json.dumps('true')
