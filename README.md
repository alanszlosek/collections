Collections
====

This project has a few purposes:

* Provide an idempotent way to back up images to Backblaze B2
* Provide a way to browse, tag and share those images

![Collections](https://alanszlosek.com/files/collections.png)

# Usage

Below are general sketches of how to use this project. Definitely a work in progress.

## Uploading Images

`src/upload.py` will calculate a unique name for your image files and upload them to b2. If it's encountered a file before, it'll skip it.

If you're like me, you have folders spread across computers containing images from old phones, camera, etc. I can run this script against any folder I encounter to be sure the images are backed up.

It's not perfect, but it's easier than resolving naming collisions manually (like when your camera rolls back around and starts naming files IMG_0001.JPG).

## Browsing Images

I've included a sample configuration file for the Caddy HTTP server in `etc/`.

1. Install pip3 dependencies: `pip3 install -r requirements.txt`
1. Create an sqlite3 database: `sqlite3 collections.sqlite3 < database/schema-sqlite3`
1. Copy `.env-example` to `.env` and fill in the appropriate values
1. `python src/app.py` to run the webapp
1. Browse to the appropriate domain and port for your app
