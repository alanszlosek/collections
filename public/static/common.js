
var getFormData = function(container) {
    var out = {};
    container.querySelectorAll('input,textarea,.selected').forEach(function(el) { //:text,:checkbox:checked,:radio:checked,select,input[type=date],input[type=time],input[type=number]').forEach(function() {

        var name;
        var value;

        if (el.hasAttribute('name')) {
            name = el.name;
            value = el.value;

        } else if(el.hasAttribute('data-value')) {
            name = el.parentNode.getAttribute('data-name');
            value = el.getAttribute('data-value');
        }

        if (name in out) {
            // We don't have checkboxes, or multi-val fields
            // Don't squash existing values. Might be a .selectable and input tag in form, just like addNoteTags
        }
        else {
            out[name] = value;
        }
    });
    return out;
};

/*
    tag('div',

        tag('div',
        ).setAttribute('class', 'selected')

    )
*/

var tag = function(tagName, attributes) {
    var args = Array.prototype.slice.call(arguments);
    tagName = args.shift();
    attributes = args.shift();

    var element = document.createElement(tagName);
    for (var i in attributes) {
        element.setAttribute(i, attributes[i]);
    }
    // Convert text to text node
    for (var i = 0; i < args.length; i ++) {
        var node = args[i];
        if (node == null) {
            continue;
        } else if (node instanceof Node) {
        } else {
            node = document.createTextNode(args[i]);
        }
        element.appendChild(node);
    }

    return element;
};

var drawChildren = function(container, children) {
    /*
    More room for cool optimizations here:
    - loop through current and desired children, compare using node types, merge differences if possible
    */
    // perhaps compare element ids

    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
    children.forEach(function(item) {
        if (item == null) {
            return;
        }
        container.appendChild(item);
    });
};


/*
var generate = function() {
    var args = Array.prototype.slice.call(arguments);
    for (var i = 0; i < arguments.length; i++) {
        var arg = arguments[i];

        if (Array.isArray(arg)) {
        } else if (arg instanceof Object) {
        } else {
            var element = document.createElement(arg);
            for (var i in attributes) {
                element.setAttribute(i, attributes[i]);
            }
        }
    }
    
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
    children.forEach(function(item) {
        if (item == null) {
            return;
        }
        container.appendChild(item);
    });

var tag = function(tagName, attributes) {
    tagName = args.shift();
    attributes = args.shift();

    // Convert text to text node
    for (var i = 0; i < args.length; i ++) {
        var node = args[i];
        if (node == null) {
            continue;
        } else if (node instanceof Node) {
        } else {
            node = document.createTextNode(args[i]);
        }
        element.appendChild(node);
    }

    return element;
};
*/


var ajax = {
    get: function(url, callback) {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);

        request.addEventListener('load', function() {
            if (request.status >= 200 && request.status < 400) {
                try {
                    var data = JSON.parse(request.responseText);
                    callback(null, data);
                } catch (e) {
                    callback("JSON parse: " + e.message);
                }
            } else {
                // We reached our target server, but it returned an error
                callback('Did not get 20x or 30x HTTP status');
            }
        });

        request.addEventListener('error', function(event) {
            callback('GET failed. Did we lose connectivity?');
        });

        request.send();
    },

    post: function(url, data, callback) {
        var request = new XMLHttpRequest();
        request.open('POST', url, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.addEventListener('load', function() {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                var data = JSON.parse(request.responseText);
                callback(null, data);
            } else {
                // We reached our target server, but it returned an error
                callback('Did not get 20x or 30x HTTP status');
            }
        });

        request.addEventListener('error', function(event) {
            callback('POST failed. Did we lose connectivity?');
        });
        request.send(JSON.stringify(data));
    }
};
