#!/bin/bash
export DB="/var/www/collections/database/test.sqlite"
rm -rf $DB
sqlite3 "${DB}" < database/schema-sqlite3
source ./venv/bin/activate
python src/fetch.py
